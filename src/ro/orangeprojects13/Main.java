package ro.orangeprojects13;

public class Main {

    public static void main(String[] args) {

        Person p1 = new Hotel_Employee("Ana Colibasa", "Female",0);
        Person p2 = new Hotel_Employee("Tiberiu Stan","Male",1);

        p1.work();
        p2.work();

        p1.changeName("Sebastian Odoherty");
        System.out.println(p1.toString());

    }
}
