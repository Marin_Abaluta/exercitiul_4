package ro.orangeprojects13;

public class Hotel_Employee extends Person{

    private int empId = 0;
    public Hotel_Employee(String name, String gender, int Id){

        super(name,gender);
        this.empId = Id;
    }

    @Override
    public void work() {

        if(this.empId == 0) {

            System.out.println("Working as hotel employee!!");

        }

    }
}
